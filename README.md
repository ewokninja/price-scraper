### Black Friday Price Scraper

Install requirements:
`pip3 install -r requirements.txt`
Run `python3 price_scraper.py`

Also requires ChromeDriver which can be downloaded from: `https://chromedriver.chromium.org/downloads`
The binary for ChromeDriver needs to be in the PATH, I recomment `~/bin/`
